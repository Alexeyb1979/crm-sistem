const Cl = (function(){
    const clients = [];

    /**
     * @desc Add client
     **/
    function addClient(name) {
        clients.push(name);
    }

    /**
     * @desc Returns the client list
     **/
    function getClients() {
        return clients;
    }

    /**
     * @desc Remove item
     * @param {String} name
     * @return {Boolean}
     **/
    function removeClient(name) {
        const index = clients.findIndex(name);

        if (index >= 0) {
            clients.splice(index, 1);

            return true;
        }

        return false;
    }

    return {
        add: addClient,
        get: getClients,
        remove: removeClient
    };
})();